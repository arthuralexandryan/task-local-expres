package com.example.tasklocalexpres.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "my_model")
data class MyModelEntity(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    var title: String?,
    var image: String?
)
