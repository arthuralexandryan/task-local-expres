package com.example.tasklocalexpres.db

import android.graphics.Bitmap
import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.tasklocalexpres.models.MyModel

@Dao
interface MyModelDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(my_model: List<MyModelEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertItem(my_model: MyModelEntity)

    @Query("SELECT * FROM my_model")
    fun getMyModels(): PagingSource<Int, MyModelEntity>

    @Query("SELECT * FROM my_model WHERE id = :position")
    fun getMyModelById(position: Int): MyModelEntity

    @Query("UPDATE my_model SET title = :title WHERE id = :id")
    suspend fun updateTitleById(id: Int, title: String): Int

    @Query("UPDATE my_model SET image = :image WHERE id = :id")
    suspend fun updateImageById(id: Int, image: String): Int
}