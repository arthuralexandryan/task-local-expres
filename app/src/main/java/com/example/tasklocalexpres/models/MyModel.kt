package com.example.tasklocalexpres.models

import android.graphics.Bitmap
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MyModel(val title: String?, val image: String?): Parcelable
