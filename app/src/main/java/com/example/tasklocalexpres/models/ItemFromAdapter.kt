package com.example.tasklocalexpres.models

import com.example.tasklocalexpres.utils.ActionItemType

data class ItemFromAdapter(
    var actionType: ActionItemType,
    var changedTitle: String?,
    var id: Int
)