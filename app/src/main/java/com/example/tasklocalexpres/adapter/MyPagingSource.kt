package com.example.tasklocalexpres.adapter

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.tasklocalexpres.db.MyModelDao
import com.example.tasklocalexpres.models.MyModel
import com.example.tasklocalexpres.repository_impl.MyRepositoryImpl
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect

class MyPagingSource(
    private val myModelDao: MyModelDao
): PagingSource<Int, MyModel>() {
    override fun getRefreshKey(state: PagingState<Int, MyModel>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, MyModel> {
        val nextPageNumber = params.key ?: 1


        val list= myModelDao.getMyModels()


        return LoadResult.Page(
            data = listOf(),
            prevKey = null,
            nextKey = nextPageNumber + 1
        )
    }
}