package com.example.tasklocalexpres.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.example.tasklocalexpres.R
import com.example.tasklocalexpres.db.MyModelEntity
import com.example.tasklocalexpres.models.ItemFromAdapter

class MyModelAdapter : PagingDataAdapter<MyModelEntity, MyModelViewHolder>(UserComparator) {
    var onActionFromItemAdapter: ((ItemFromAdapter) -> Unit)? = null

    override fun onBindViewHolder(holder: MyModelViewHolder, position: Int) {
        holder.onBind(getItem(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyModelViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_card, parent, false)
        return MyModelViewHolder(view)
    }

    override fun onViewAttachedToWindow(holder: MyModelViewHolder) {
        holder.onAction = { title, id, actionType ->
            onActionFromItemAdapter?.invoke(ItemFromAdapter(actionType, title, id))
        }
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    companion object UserComparator : DiffUtil.ItemCallback<MyModelEntity>() {
        override fun areItemsTheSame(oldItem: MyModelEntity, newItem: MyModelEntity): Boolean {
            return oldItem.image == newItem.image
        }

        override fun areContentsTheSame(oldItem: MyModelEntity, newItem: MyModelEntity): Boolean {
            return oldItem.image == newItem.image
        }
    }
}
