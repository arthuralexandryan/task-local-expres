package com.example.tasklocalexpres.adapter

import android.view.View
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.tasklocalexpres.R
import com.example.tasklocalexpres.db.MyModelEntity
import com.example.tasklocalexpres.utils.ActionItemType
import com.example.tasklocalexpres.utils.ImageBitmapString

class MyModelViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

    var onAction: ((String?, Int, ActionItemType) -> Unit)? = null
    private var title: AppCompatEditText? = null
    private var image: AppCompatImageView? = null

    fun onBind(item: MyModelEntity?) {
        initViews()
        title?.setText(item?.title)
        title?.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus && (v as? AppCompatEditText)?.text?.isNotEmpty() == true) {
                item?.id?.run { onAction?.invoke(v.text.toString(), this, ActionItemType.TITLE) }
            }
        }
        image?.run {
            item?.image?.run {
                setImageBitmap(ImageBitmapString.stringToBitMap(this))
            }
            setOnClickListener {
                item?.id?.run { onAction?.invoke(null, this, ActionItemType.IMAGE) }
            }
        }
    }

    private fun initViews() {
        title = view.findViewById(R.id.title)
        image = view.findViewById(R.id.avatar)
    }
}