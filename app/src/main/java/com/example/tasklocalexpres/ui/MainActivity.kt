package com.example.tasklocalexpres.ui

import android.content.ContentResolver
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.ProgressBar
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.tasklocalexpres.AppClass
import com.example.tasklocalexpres.R
import com.example.tasklocalexpres.adapter.MyModelAdapter
import com.example.tasklocalexpres.models.ItemFromAdapter
import com.example.tasklocalexpres.models.MyModel
import com.example.tasklocalexpres.utils.ActionItemType
import com.example.tasklocalexpres.utils.ImageBitmapString
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import java.io.FileDescriptor
import java.io.IOException


class MainActivity : AppCompatActivity() {

    private val adapter = MyModelAdapter()
    private val pickImage = 100
    private var imageUri: Uri? = null
    private lateinit var itemAdapter: ItemFromAdapter
    private lateinit var recyclerView: RecyclerView

    private val viewModel: MainViewModel by viewModels {
        MyViewModelsFactory((application as AppClass).myRepositoryImpl)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initRecyclerView()
        getAllModels()
        initAddModelClick()
        initItemAction()
    }

    private fun initItemAction() {
        adapter.onActionFromItemAdapter = { item ->
            itemAdapter = item
            when (item.actionType) {
                ActionItemType.TITLE -> {
                    item.changedTitle?.run { updateModelItemTitle(item.id, this) }
                }
                ActionItemType.IMAGE -> {
                    val gallery =
                        Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
                    startActivityForResult(gallery, pickImage)
                }
            }
        }
    }

    private fun uriToBitmap(selectedFileUri: Uri): Bitmap? {
        return try {
            val parcelFileDescriptor = contentResolver.openFileDescriptor(selectedFileUri, "r")
            val fileDescriptor: FileDescriptor = parcelFileDescriptor!!.fileDescriptor
            val image = BitmapFactory.decodeFileDescriptor(fileDescriptor)
            parcelFileDescriptor.close()
            image
        } catch (e: IOException) {
            e.printStackTrace()
            null
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == pickImage) {
            imageUri = data?.data
            imageUri?.let {
                updateModelItemImage(
                    itemAdapter.id,
                    ImageBitmapString.bitMapToString(uriToBitmap(it))
                )
            }
        }
    }

    private fun initAddModelClick() {
        findViewById<AppCompatImageView>(R.id.add_item).setOnClickListener {
            addModelItem()
        }
    }

    private fun addModelItem() {
        lifecycleScope.launchWhenResumed {
            viewModel.addModel(
                MyModel(
                    "test${adapter.itemCount}",
                    null
                )
            ).collect {
                adapter.refresh()
                recyclerView.scrollToPosition(adapter.snapshot().size - 1)
            }
        }
    }


    private fun updateModelItemTitle(id: Int, title: String) {
        lifecycleScope.launchWhenResumed {
            viewModel.updateModelTitleById(id, title).collect {
                it?.run {
                    adapter.refresh()
                }
            }
        }
    }

    private fun updateModelItemImage(id: Int, image: String?) {
        lifecycleScope.launchWhenResumed {
            image?.run {
                viewModel.updateModelImageById(id, image).collect {
                    it?.run {
                        adapter.refresh()
                    }
                }
            }
        }
    }

    private fun getAllModels() {
        lifecycleScope.launchWhenResumed {
            viewModel.getModels().collectLatest {
                adapter.submitData(it)
            }
        }
    }

    private fun initRecyclerView() {
        recyclerView = findViewById(R.id.my_recycler)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = GridLayoutManager(this, 2)
    }
}