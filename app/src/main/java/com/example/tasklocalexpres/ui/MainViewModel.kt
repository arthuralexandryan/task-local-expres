package com.example.tasklocalexpres.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import com.example.tasklocalexpres.db.MyModelEntity
import com.example.tasklocalexpres.models.MyModel
import com.example.tasklocalexpres.repository_impl.MyRepositoryImpl
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch

class MainViewModel(
    private val myRepositoryImpl: MyRepositoryImpl
) : ViewModel() {
    suspend fun getModels(): StateFlow<PagingData<MyModelEntity>> {
        return myRepositoryImpl.getMyModelPaging.stateIn(viewModelScope)
    }

    fun addModel(model: MyModel): StateFlow<Any?> {
        val data = MutableStateFlow<Any?>(null)
        viewModelScope.launch {
            data.value = myRepositoryImpl.insertMyModel(model)
        }
        return data
    }

    fun updateModelTitleById(position: Int, title: String): StateFlow<Int?>  {
        val data = MutableStateFlow<Int?>(null)
        viewModelScope.launch {
            myRepositoryImpl.updateItemTitle(position, title).collect {
                data.value = it
            }
        }
        return data
    }

    fun updateModelImageById(id: Int, image: String): StateFlow<Int?>  {
        val data = MutableStateFlow<Int?>(null)
        viewModelScope.launch {
            myRepositoryImpl.updateItemImage(id, image).collect {
                data.value = it
            }
        }
        return data
    }
}