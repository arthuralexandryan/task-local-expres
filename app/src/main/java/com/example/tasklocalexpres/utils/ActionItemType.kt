package com.example.tasklocalexpres.utils

enum class ActionItemType(val type: String) {
    IMAGE("image"),
    TITLE("title")
}