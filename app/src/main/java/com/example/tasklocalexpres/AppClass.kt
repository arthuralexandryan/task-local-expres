package com.example.tasklocalexpres

import android.app.Application
import com.example.tasklocalexpres.db.AppDatabase
import com.example.tasklocalexpres.repository_impl.MyRepositoryImpl
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class AppClass: Application() {
    private val applicationScope = CoroutineScope(SupervisorJob())
    private val appDB by lazy { AppDatabase.getDatabase(this, applicationScope) }
    val myRepositoryImpl by lazy { MyRepositoryImpl(appDB.myModelDao()) }
}