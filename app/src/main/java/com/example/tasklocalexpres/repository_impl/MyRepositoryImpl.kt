package com.example.tasklocalexpres.repository_impl

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingSource
import com.example.tasklocalexpres.db.MyModelDao
import com.example.tasklocalexpres.db.MyModelEntity
import com.example.tasklocalexpres.models.MyModel
import com.example.tasklocalexpres.repository.MyRepository
import kotlinx.coroutines.flow.flowOf

class MyRepositoryImpl(
    private val myModelDao: MyModelDao
    ): MyRepository {
    override suspend fun insertMyModel(model: MyModel) = flowOf(myModelDao.insertItem(
        MyModelEntity(title = model.title, image = model.image)))

    override fun getMyModelsLocal(): PagingSource<Int, MyModelEntity> = myModelDao.getMyModels()

    override suspend fun updateItemTitle(id: Int, title: String) = flowOf(myModelDao.updateTitleById(id, title))

    override suspend fun updateItemImage(id: Int, image: String) = flowOf(myModelDao.updateImageById(id, image))

    val getMyModelPaging = Pager(
        PagingConfig(pageSize = 10, initialLoadSize = 10)
    ) {
        myModelDao.getMyModels()
    }.flow
}