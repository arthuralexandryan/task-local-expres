package com.example.tasklocalexpres.repository

import android.graphics.Bitmap
import androidx.paging.PagingSource
import com.example.tasklocalexpres.db.MyModelEntity
import com.example.tasklocalexpres.models.MyModel
import kotlinx.coroutines.flow.Flow

interface MyRepository {
    suspend fun insertMyModel(model: MyModel): Flow<Any>

    fun getMyModelsLocal(): PagingSource<Int, MyModelEntity>

    suspend fun updateItemTitle(id: Int, title: String): Flow<Int>?

    suspend fun updateItemImage(id: Int, image: String): Flow<Int>?
}