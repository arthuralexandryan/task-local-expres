package com.example.tasklocalexpres.constant

import com.example.tasklocalexpres.db.MyModelEntity

object AppConstant {
    const val DB_NAME = "my_database"
    val list = arrayListOf<MyModelEntity>().apply {
        for (i in 0..200){
            this.add(MyModelEntity(title = "test$i", image = null))
        }
    }
}